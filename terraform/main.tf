terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.13.0"
    }
  }
}



provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "ansible-playground" {
  name     = "ansible-playground-resources"
  location = "West Europe"
}


resource "azurerm_virtual_network" "ansible-playground" {
  name                = "ansible-playground-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.ansible-playground.location
  resource_group_name = azurerm_resource_group.ansible-playground.name
}

resource "azurerm_subnet" "ansible-playground" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.ansible-playground.name
  virtual_network_name = azurerm_virtual_network.ansible-playground.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "ansible-playground" {
  count               = 3
  name                = "pip-${count.index}"
  resource_group_name = azurerm_resource_group.ansible-playground.name
  location            = azurerm_resource_group.ansible-playground.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "ansible-playground" {
  count               = 3
  name                = "ansible-playground-nic-${count.index}"
  location            = azurerm_resource_group.ansible-playground.location
  resource_group_name = azurerm_resource_group.ansible-playground.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.ansible-playground.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.ansible-playground[count.index].id
  }
}

resource "azurerm_linux_virtual_machine" "ansible-playground" {
  count               = 3
  name                = "ansible-playground-machine-${count.index}"
  resource_group_name = azurerm_resource_group.ansible-playground.name
  location            = azurerm_resource_group.ansible-playground.location
  size                = "Standard_F2"
  admin_username      = "ansible"
  network_interface_ids = [
    azurerm_network_interface.ansible-playground[count.index].id,
  ]

  admin_ssh_key {
    username   = "ansible"
    public_key = file("~/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "almalinux"
    offer     = "almalinux"
    sku       = "8-gen1"
    version   = "latest"
  }

  plan {
    name = "8-gen1"
    publisher = "almalinux"
    product = "almalinux"
  }
}

data "azurerm_public_ip" "ansible-playground" {
  count =3
  name                = azurerm_public_ip.ansible-playground[count.index].name
  resource_group_name = azurerm_resource_group.ansible-playground.name
  depends_on          = [azurerm_linux_virtual_machine.ansible-playground]
}


resource "local_file" "hosts_inventory" {
  depends_on = [
    azurerm_network_interface.ansible-playground
  ]
  content = templatefile("${path.module}/inventory.tpl",
    {
      pips = data.azurerm_public_ip.ansible-playground[*].ip_address
    }
  )
  filename = "../ansible/inventory"
}
